bump2version==1.0.0
coverage==5.1
coverage_enable_subprocess==1.0
check-manifest==0.42
conventional-commit==0.3.3
flake8==3.7.9
git-changelog==0.3.0
mock==4.0.2
pytest==5.4.1
pytest-cov==2.8.1
pytest-django==3.9.0
python-gitlab==2.2.0
recommonmark
sphinx==3.0.3
sphinx-rtd-theme==0.4.3
tox==3.15.0
tox-venv==0.4.0
twine==3.1.1
wheel
