# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [v5.1.1](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.1.1) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.19...v5.1.1)) - 2020-07-09

### Misc
- "[skip ci] bump version: 5.1.0 → 5.1.1" ([bbd5abe](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/bbd5abebdd004cf7501ff320974b967da129673c) by lordoftheflies).
- [skip ci] update changelog for \#631796594. ([5f6977c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/5f6977c5e196f74c635c8ccc9941151cbd44a8fa) by lordoftheflies).
- Revert "chore(node): move typescrpt modules to es6 modules." ([94537cb](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/94537cb0b080ce9b85f54504429706412b24e1d5) by lordoftheflies).
- Chore(node): move typescrpt modules to es6 modules. ([99faac1](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/99faac104591fb795449b994615e0f7d02bbbbff) by lordoftheflies).
- "[skip ci] bump version: 5.0.19 → 5.1.0" ([326a451](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/326a4516f0169ef1a3e0e9ec9e22900d92a632a0) by lordoftheflies).


## [v5.0.19](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.19) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.18...v5.0.19)) - 2020-07-08

### Misc
- "[skip ci] bump version: 5.0.18 → 5.0.19" ([f42a9e3](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f42a9e31c4d587ad80e99bde0ce13fbe98aef571) by lordoftheflies).
- [skip ci] update changelog for \#628382172. ([b457946](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b4579461e8fa87561d233c6146822f22becad50c) by lordoftheflies).
- Chore(node): fix modules path ([b817870](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b817870546814a6a85acc303cac48c818deaed6d) by lordoftheflies).


## [v5.0.18](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.18) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.17...v5.0.18)) - 2020-07-03

### Misc
- "[skip ci] bump version: 5.0.17 → 5.0.18" ([01d31d1](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/01d31d1b7dbbf638a1e3d4601c43e7cfb094a047) by lordoftheflies).
- [skip ci] update changelog for \#622620780. ([26de23f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/26de23f36d6da816474e33bf55cd9b3904f7b041) by lordoftheflies).
- Chore(ci): fix pipeline. ([7463f8d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/7463f8df10b8754e489a8eb6bf39378db6c0c48a) by lordoftheflies).
- Chore(node): include material-web-components requirements. ([b5cd7bc](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b5cd7bc2f234270bf263c2eb323fdadb0a3e824f) by lordoftheflies).
- Chore(node): add package.json to the app ([f2a4730](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f2a4730af5776a3c1bf4ae458245c4001af9df57) by lordoftheflies).
- Chore(node): add npm install to the pipeline, and the django configuration. ([4eb8459](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4eb84594de4576f4ce6cbfabed5ab516e9ef29c1) by lordoftheflies).
- Chore(node): ignore node module directory. ([3637cee](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/3637ceecffcd21902325cfb65371d8a8f03f3f82) by lordoftheflies).


## [v5.0.17](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.17) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.16...v5.0.17)) - 2020-07-01

### Misc
- "[skip ci] bump version: 5.0.16 → 5.0.17" ([2f9c40b](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/2f9c40b919bfec2d6792b8f5a13b25bfe2ec48fe) by lordoftheflies).
- [skip ci] update changelog for \#619740700. ([82a739d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/82a739d8781557666a0ff9214443976de090b60e) by lordoftheflies).
- Chore(ci): fixes for demo ([f87126c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f87126cba02d789f32d98e3128bdd0a013e8e522) by lordoftheflies).


## [v5.0.16](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.16) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.15...v5.0.16)) - 2020-07-01

### Misc
- "[skip ci] bump version: 5.0.15 → 5.0.16" ([1894118](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/18941186a0ed25799f8a95fa5c98a512f971d25b) by lordoftheflies).
- [skip ci] update changelog for \#619371275. ([0a0f9c9](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/0a0f9c9737b53b3c7d46705316c84b22704e1acb) by lordoftheflies).


## [v5.0.15](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.15) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.14...v5.0.15)) - 2020-07-01

### Misc
- "[skip ci] bump version: 5.0.14 → 5.0.15" ([79229a8](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/79229a84f83634b45939e4f0a6b1e8e947852914) by lordoftheflies).
- [skip ci] update changelog for \#619251286. ([ccb73ff](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/ccb73ff9f36ed3ecef9899fc4b6585c2b34556b4) by lordoftheflies).


## [v5.0.14](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.14) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.13...v5.0.14)) - 2020-07-01

### Misc
- "[skip ci] bump version: 5.0.13 → 5.0.14" ([5db3485](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/5db3485ad755a1d831c39b0e8c24faab8db7de97) by lordoftheflies).
- [skip ci] update changelog for \#618930094. ([a618a6f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/a618a6f61ee702dca3eb11ae848d92696bae573e) by lordoftheflies).


## [v5.0.13](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.13) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.12...v5.0.13)) - 2020-07-01

### Fixed
- Fix(test): make migrations. ([fd7da4b](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/fd7da4b5bd48085e425ba08e1220eb183c1ef52a) by lordoftheflies).
- Fix(mdm): migráció ([7f1e989](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/7f1e98918be0aa472e24820e9f6b85e2fc25efc9) by lordoftheflies).

### Misc
- "[skip ci] bump version: 5.0.12 → 5.0.13" ([b0ea496](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b0ea496e854a684b4f73944daa9b312a2fc26c3f) by lordoftheflies).
- [skip ci] update changelog for \#618754932. ([4d362ee](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4d362ee887f76a2e142905120726ff8e2644c0cd) by lordoftheflies).
- Feat(mdm): osztalékok ([fd5ced5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/fd5ced51f46231470cfce52fb2725d86267b81a1) by lordoftheflies).


## [v5.0.12](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.12) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.11...v5.0.12)) - 2020-06-30

### Misc
- "[skip ci] bump version: 5.0.11 → 5.0.12" ([e8f4ca0](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e8f4ca07623c8c35a3178db0fc3df9b2f9b33502) by lordoftheflies).
- [skip ci] update changelog for \#618079417. ([7ab5617](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/7ab5617daf9191266d839ae7fc7c18c6da57762d) by lordoftheflies).
- Chore(demo): kezdőlap link a workflowra dob ([9b3e797](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/9b3e797b63c9211646268c2666d31cd2d246508a) by lordoftheflies).
- Feat(mdm): bér - jövedelelmek ([a59425e](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/a59425e8f2f41e91de07bb2419790cda1b4c651e) by lordoftheflies). Related issues/PRs: #165


## [v5.0.11](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.11) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.10...v5.0.11)) - 2020-06-29

### Misc
- "[skip ci] bump version: 5.0.10 → 5.0.11" ([76974c7](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/76974c732f5c137b116a4180b70ce7dbf0a1e9bd) by lordoftheflies).
- [skip ci] update changelog for \#616319097. ([4eeee55](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4eeee55a057a8696c3d658839e448f0debabcaea) by lordoftheflies).
- Chore(ci): ignore triggers. ([4298494](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/42984945d2066d9016fe595707c6835f086dee49) by lordoftheflies).


## [v5.0.10](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.10) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.9...v5.0.10)) - 2020-06-29

### Misc
- "[skip ci] bump version: 5.0.9 → 5.0.10" ([2f6ccfd](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/2f6ccfd07105b24766376b5197ea100f678f10e4) by lordoftheflies).
- [skip ci] update changelog for \#616251813. ([305e563](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/305e5630689ed9f97c246cf83498aa995d2a500a) by lordoftheflies).


## [v5.0.9](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.9) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.8...v5.0.9)) - 2020-06-28

### Misc
- "[skip ci] bump version: 5.0.8 → 5.0.9" ([e04ef4f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e04ef4f74439b0abd508895cb1fa049d1ba31c00) by lordoftheflies).
- [skip ci] update changelog for \#614493494. ([a786091](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/a78609169982e1a8d0f707eb087b2a0ae6c20df8) by lordoftheflies).


## [v5.0.8](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.8) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.7...v5.0.8)) - 2020-06-27

### Misc
- "[skip ci] bump version: 5.0.7 → 5.0.8" ([e491d6a](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e491d6abe445d1b1d54df95f02e0abd6ffda2173) by lordoftheflies).
- [skip ci] update changelog for \#614252463. ([9a02212](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/9a022123fa0294fa1f8c19af38bb18a69029fb0b) by lordoftheflies).


## [v5.0.7](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.7) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.6...v5.0.7)) - 2020-06-25

### Misc
- "[skip ci] bump version: 5.0.6 → 5.0.7" ([b68894e](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b68894e0ba796facfd3ef36a7c7cbac23d32d315) by lordoftheflies).
- [skip ci] update changelog for \#610622104. ([0c3e510](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/0c3e510a93768488968d5416cb1a80813ebab853) by lordoftheflies).
- Stage ([7ec7a84](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/7ec7a84560e7010de14edbd8194517e1d9f86c1b) by lordoftheflies).
- Feat(timesheet): refactoring ([053fc78](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/053fc788a1ee4336908862cf914be6f670408137) by lordoftheflies).


## [v5.0.6](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.6) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.5...v5.0.6)) - 2020-06-24

### Fixed
- Fix(mdm): fix date range. ([a4c0967](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/a4c096749311c020c0f6513d443a13ff783266c1) by lordoftheflies).

### Misc
- "[skip ci] bump version: 5.0.5 → 5.0.6" ([3e20038](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/3e200387272c25ff2a6a5a2a693307aad69d5565) by lordoftheflies).
- [skip ci] update changelog for \#609785915. ([eb49d7f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/eb49d7fbe14ae683b8a8a2c7186f7957e6c28d32) by lordoftheflies).
- Feat(mdm): eltartás lista ([2c61219](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/2c612193d50c935226ad7a2a289b51d1c2de402a) by lordoftheflies). Related issues/PRs: #161


## [v5.0.5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.5) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.4...v5.0.5)) - 2020-06-24

### Fixed
- Fix(ci): upgrade generator. ([1e216c2](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1e216c2159aa08141132d720ba33becda4633069) by lordoftheflies).

### Misc
- "[skip ci] bump version: 5.0.4 → 5.0.5" ([d8d7445](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d8d7445cbbc6888e06744edbf1aaddf1837e84ec) by lordoftheflies).
- [skip ci] update changelog for \#609329018. ([e2c77aa](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e2c77aa35b0bda8c27adeaf25a9aac2b2b8e199c) by lordoftheflies).
- Chore(ci): fix ([e536c37](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e536c3728dc4fac675dd517e2f562188f9bad04a) by lordoftheflies).
- Feat(payroll): letiltások ([e54ae4c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e54ae4c293ed83699690833466a88c0cdce2ebc7) by lordoftheflies). Related issues/PRs: #159
- Chore(ci): hard-code initial roles. ([beb1862](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/beb18620327645d631a1539fbb2269f51f06357a) by lordoftheflies).
- Chore(perf): add roles. ([4cc7d08](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4cc7d08bb55f3dc4ab27639112e6b390c53eb247) by lordoftheflies).


## [v5.0.4](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.4) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.3...v5.0.4)) - 2020-06-23

### Misc
- "[skip ci] bump version: 5.0.3 → 5.0.4" ([a457661](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/a457661e8251773d7104aabe5e49ba6110b413e2) by lordoftheflies).
- [skip ci] update changelog for \#606993935. ([10b789c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/10b789ca2cce3bb08cab755eb4b2110312ce9731) by lordoftheflies).
- Stg ([df85edf](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/df85edf2b27d1825806913c9456a058e98a5e9fe) by lordoftheflies).


## [v5.0.3](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.3) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v5.0.2...v5.0.3)) - 2020-06-19

### Misc
- "[skip ci] bump version: 5.0.2 → 5.0.3" ([5942307](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/59423076dd3b8dfb54e7104430a554b4efe158c1) by lordoftheflies).
- [skip ci] update changelog for \#603548766. ([1a51b43](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1a51b43e14f2fa9e94af5a38d99601516ec89a75) by lordoftheflies).
- Chire(ci): build triggers. ([b475849](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b4758492bfaf06c339ea99bd90d91d96e76536c5) by lordoftheflies).


## [v5.0.2](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v5.0.2) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.9...v5.0.2)) - 2020-06-19

### Fixed
- Fix(typho): dátum generálás minta adatokban. ([8f5a9b5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/8f5a9b5d8bb82e04db9098fc69162c94eaf93e7c) by lordoftheflies).
- Fix(typho): adokedvezmény typho ([64cd891](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/64cd8911ffac7fc113b2437289e0955c51f8c64d) by lordoftheflies).
- Fix(management): fix customize command ([93f3f0c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/93f3f0c8d6c5838a8d00bf2ddeedb1db34f4ee04) by lordoftheflies).

### Misc
- "[skip ci] bump version: 5.0.1 → 5.0.2" ([94a6d5d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/94a6d5dc8dd7310b54ed781beac07b0d633447e9) by lordoftheflies).
- [skip ci] update changelog for \#603203240. ([60102fa](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/60102fa0c0c382ac0da79205156cde5b8e36f756) by lordoftheflies).
- "[skip ci] bump version: 5.0.0 → 5.0.1" ([f5543ce](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f5543ce60f1a719f355a6029b2170cf210b8ece3) by lordoftheflies).
- Chore(e2e): fix xutomize command ([60c32e3](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/60c32e3782895a6310cd34211aff6b673e99b303) by lordoftheflies).
- Chore(ci): move migration ([cc66a69](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/cc66a69903d86679d9b8892a13bd5fd4280610e3) by lordoftheflies).
- Chore(mdm): személyi adatlapok ([046c983](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/046c9836a89b3a305a2379a85704a0bd5cd5774d) by lordoftheflies).
- "[skip ci] bump version: 4.0.0 → 5.0.0" ([bff7f15](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/bff7f154db684e1bf5190aa72e1775784be3d3f1) by lordoftheflies).
- "[skip ci] bump version: 3.0.0 → 4.0.0" ([c2bc983](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/c2bc98327f0fc1596d19837b2aa9c9e17b280130) by lordoftheflies).
- "[skip ci] bump version: 2.0.0 → 3.0.0" ([321c8d5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/321c8d54b22354bb79416fdf7e200395647ea597) by lordoftheflies).
- "[skip ci] bump version: 1.0.8 → 2.0.0" ([0610578](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/061057804369827d930b96ef6aa77d8a254b2d55) by lordoftheflies).


## [v1.0.9](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.9) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.8...v1.0.9)) - 2020-06-17

### Misc
- "[skip ci] bump version: 1.0.8 → 1.0.9" ([d002b13](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d002b132e6168337b755541ff851803584fb90fc) by lordoftheflies).
- [skip ci] update changelog for \#598873245. ([f4657c0](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f4657c048d2ee6fce0703ee455cedc4e722f5a1f) by lordoftheflies).
- Chore(ac): add admin portions. ([dbd24b7](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/dbd24b7bdfca216d3c962b71a316add14f3f02a3) by lordoftheflies).


## [v1.0.8](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.8) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.7...v1.0.8)) - 2020-06-16

### Misc
- "[skip ci] bump version: 1.0.7 → 1.0.8" ([eeddc36](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/eeddc36ce5113026ac0d11be3890c0c92d42bc17) by lordoftheflies).
- [skip ci] update changelog for \#598294188. ([32790bf](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/32790bf39ef99d14fd183835f0d217a0ca551927) by lordoftheflies).
- Chore(perf): fix templates. ([52aa108](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/52aa108d84b588d9af7bdee1f1d6140ebd3b97de) by lordoftheflies).
- Chore(ci): fix testenv. ([1cd3e86](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1cd3e86792ba903730611d4fbad671fede79056b) by lordoftheflies).
- Chore(test): fix unit tests. ([d49a7b4](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d49a7b4847474e638a54d56d5c5f25091381489f) by lordoftheflies).
- Chore(mdm): cosmetics ([0f3404a](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/0f3404a92dd4324d3ffb3e4a878da23574262d61) by lordoftheflies).
- Feat(mdm): ekho limit törzs ([82cf311](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/82cf311490b946efe7470a06bdfd104d86e9b213) by lordoftheflies). Related issues/PRs: #117
- Feat(mdm): első házasok nyilatkozat törzs ([d92d8cf](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d92d8cfb4054df2b64c28cd0cec1ebd041aa27cb) by lordoftheflies). Related issues/PRs: #115
- Chore(mdm): fixes #113 ([1c42b1f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1c42b1f33853e2e81097fe982bbaca5c85ef65b1) by lordoftheflies).
- Chore(mdm): jövedelem típusa ([642ab09](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/642ab09853f0615fd1ec5d7eb62abe2dfd44b604) by lordoftheflies). Related issues/PRs: #112
- Chore(ci):  cosmetics ([c1a9788](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/c1a9788c29385aad017b4b7c44aed82f33aefa75) by lordoftheflies).
- Feat(mdm): önálló tevékenység nyilatkozat törzs ([44aadc9](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/44aadc9ab4eeead45f46fa2f455307346f211327) by lordoftheflies). Related issues/PRs: #105
- Feat(mdm): alkalmazás minősége törzs ([43144be](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/43144be50d767e044cac0f0e78f36c383033fe4f) by lordoftheflies). Related issues/PRs: #101
- Chore(mdm): last portion of masterdata: eltartottak ([6fe0825](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/6fe0825450b916c318b12a089301996b2cad9928) by lordoftheflies).
- Chore(db): fix migration ([d95fa90](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d95fa90b0c4b43976d49c75c693e08d7220fb456) by lordoftheflies).
- Chore(ci): make node modules dir. ([f313b39](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f313b392cb6907901779209790d0be6f886e7c97) by lordoftheflies).
- Chore(ci): add title ([bbb89d2](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/bbb89d249cda66b87e2f757a55cbbdd486dd6ec6) by lordoftheflies).
- Chore(ci): fixes. ([c11444f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/c11444f46fc2d54847f94a2a608f9dcc21601d2b) by lordoftheflies).
- Chore(ci): stg ([ed31e70](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/ed31e70132e80e9fa085487ed7dfa0f44cab6c34) by lordoftheflies).
- Chore(mdm): járulék törzs creation. ([ffdc5ae](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/ffdc5aee41f2aafd2d29b63e03aac1e20da19260) by lordoftheflies).
- Chore(view): add missing serializers and views. ([07a815c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/07a815c3ca60843e6255337dd8927ab8418af974) by lordoftheflies).
- Chore(mdm): add járulék admin ([07a0c16](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/07a0c162390b7760ab318dfe5ceed31cb49d44fd) by lordoftheflies).
- Chore(env): fix cors headers. ([2e2cc45](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/2e2cc4559c74abe04b4983283b3881c741066b8a) by lordoftheflies).
- Initial url ([cdf6f43](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/cdf6f438e5e9ec044a501d327b5b0ef7b3eb7f28) by lordoftheflies).
- Chore(ui): npm support. ([1cc9442](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1cc9442979ac1f42d4c9d944fe7365290e5b4ce6) by lordoftheflies).
- Chore(ui): fix gravatar- ([48eac4c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/48eac4cfbcd22a34cf0db7c25ffb11b53a3ab0e9) by lordoftheflies).
- Chore(ui): menu change ([a59170a](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/a59170a2cc1f2903a4ed6fe1019101029e2c4a42) by lordoftheflies).
- Chore(perf): move frontend to specific module. ([3fcc1ca](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/3fcc1ca5495cf9b82301f85625eb0ed4a261e9cb) by lordoftheflies).
- Chore(ci): cosmetics. ([1b90e05](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1b90e05590fe8ae813e55e49560bd7463e78462a) by lordoftheflies).
- Chore(ci): ugrade requirements. ([8aa2152](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/8aa2152fc4ffb9fbf2239dc0f4309ddfbecbe8b0) by lordoftheflies).
- Chore(backend): separate worker unit. ([074bd02](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/074bd022126ea16c0c45d7825b8c4a6d362afa49) by lordoftheflies).
- Feat(celery): integrate celery workers. ([e64e4af](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e64e4af273195f84909a5bc04b2f2d537434b38e) by lordoftheflies). Related issues/PRs: #8
- Feat(mdm): járulék model felvétele. ([ee14cfd](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/ee14cfd0a6c77a39bd0fc0c64729b475d77b0abc) by lordoftheflies).
- Chore(ci): fix static config, hardcode producition configuration ([1c4ce9f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1c4ce9fdccc5fef7fa246df02c75cef401698d33) by lordoftheflies).
- Stage ([a7b9088](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/a7b908829d527871f401e41b4eee2975a9423a55) by lordoftheflies).
- Stage. ([9918642](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/9918642c966465403d426d56d8bf791e6511f395) by lordoftheflies).
- Chore(ui): fix tabe headers. ([75339d4](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/75339d43b324c8d97bed13d8e2e30e2234da3ef2) by lordoftheflies).
- Chore(dist): clear namespaces. ([22cfdf9](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/22cfdf93ca464e57244d3c30a55949c150121a5f) by lordoftheflies).
- Chore(masterdata): migrations. ([d376dc7](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d376dc7c6298b3388b7bc738be2f93252c623d69) by lordoftheflies).
- Chore ([00ea0e5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/00ea0e5fe7babe9ace67d4bf8abb103544bcf66f) by lordoftheflies).
- Chore(perf): fixes timesheet calculations. ([56bc26d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/56bc26dc061be946f9ed49fa51e3d5898fa28418) by lordoftheflies).
- Chore(perf): increase database verbosity. ([6dafec6](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/6dafec6f0153d2967ecaeb3fad55f9f02c1cb453) by lordoftheflies).


## [v1.0.7](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.7) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.6...v1.0.7)) - 2020-06-10

### Misc
- "[skip ci] bump version: 1.0.6 → 1.0.7" ([0be0e0d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/0be0e0d7c5520b161a1045dd4d1712922e039e40) by lordoftheflies).
- [skip ci] update changelog for \#589094172. ([e238772](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e238772406568acaef41591d2435690011294426) by lordoftheflies).
- Chore(db): merge migrations and add jogviszony. ([e64560c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e64560c882f37c02b3ae13dbb4e49732e7efdb08) by lordoftheflies).


## [v1.0.6](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.6) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.5...v1.0.6)) - 2020-06-09

### Misc
- "[skip ci] bump version: 1.0.5 → 1.0.6" ([136b62e](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/136b62e7f781366bdac86034db734a9b51d4358e) by lordoftheflies).
- [skip ci] update changelog for \#586698725. ([8b2430c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/8b2430ca033ebaa4142e19589f1c2a254b3620f0) by lordoftheflies).


## [v1.0.5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.5) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.4...v1.0.5)) - 2020-06-08

### Misc
- "[skip ci] bump version: 1.0.4 → 1.0.5" ([c06da9f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/c06da9fffcff0a18c9f013ae8ec045ca8ca50848) by lordoftheflies).
- [skip ci] update changelog for \#585819664. ([dfaab44](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/dfaab442acb3ed5b7784daca6a3181d3b1e87816) by lordoftheflies).
- Feat(uaa): externalizált felhasznűlók és szerepkörök. ([497ad0d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/497ad0ddb30017d28e46d696cc6269922d5bde01) by lordoftheflies).
- Chore(ci): remove jq from build pipleine ([4b4992d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4b4992d063eafe2459aa22c6ca57cee5868a1feb) by lordoftheflies).
- Chore(ci): jq install ([808d983](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/808d983969743f6d12bac536c9ae5bdb6b576d7b) by lordoftheflies).


## [v1.0.4](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.4) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.3...v1.0.4)) - 2020-06-08

### Fixed
- Fix(mdm): code should be unique. ([f872ba5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f872ba5380d7bdde4375a6d970042d5600e689c3) by lordoftheflies). Related issues/PRs: #127
- Fix(db): fix mdm person constrait for name. ([96883f7](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/96883f70aa6bf5f45462ee5920ddc6b1445d2861) by lordoftheflies).

### Misc
- "[skip ci] bump version: 1.0.3 → 1.0.4" ([c9b0b94](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/c9b0b94f135b69513cfacf955177e9b69ae29694) by lordoftheflies).
- [skip ci] update changelog for \#585574514. ([129b940](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/129b940da5ba424dd9331fd382b366c4b29bd1b5) by lordoftheflies).
- Perf(issue-template): törzsadat issue template. ([f13f138](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/f13f138852d1019e3ab4aeb29a70eaabc4299288) by lordoftheflies).


## [v1.0.3](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.3) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.2...v1.0.3)) - 2020-06-04

### Misc
- "[skip ci] bump version: 1.0.2 → 1.0.3" ([47b4760](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/47b476032b5a7dad996544f6beaebe179ac6a2aa) by lordoftheflies).
- [skip ci] update changelog for \#582186665. ([435cef8](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/435cef8829de6120d5899a768c420925329155d0) by lordoftheflies).
- Chore(i18n): fix locales. ([4a15bff](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4a15bff0341d011c957dcef0f676a72ae28269fc) by lordoftheflies).


## [v1.0.2](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.2) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v1.0.1...v1.0.2)) - 2020-06-03

### Misc
- "[skip ci] bump version: 1.0.1 → 1.0.2" ([902eaba](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/902eabaa678248a8cdc1253f39495f40390dac1e) by lordoftheflies).
- [skip ci] update changelog for \#579173673. ([b0fa6df](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b0fa6df18d350dedd163c4268fb741e379eb3326) by lordoftheflies).


## [v1.0.1](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v1.0.1) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v0.3.6...v1.0.1)) - 2020-06-01

### Misc
- "[skip ci] bump version: 1.0.0 → 1.0.1" ([c5d5454](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/c5d54547d09d0154a967ce70a4d0db76a1b3878e) by lordoftheflies).
- [skip ci] update changelog for \#575717873. ([efdca1b](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/efdca1b485c095e1ff25b523b01a4a3609ea31b9) by lordoftheflies).
- "[skip ci] bump version: 0.3.6 → 1.0.0" ([b7ea38a](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b7ea38a5d00ded69ea68462f5bae3febaacc878d) by lordoftheflies).
- Feat(masterdata): jogviszony és unittesztek javítása ([355ed02](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/355ed02b67daa0ae9a2281b31dd07e4643c32c72) by lordoftheflies).
- Feat(masterdata): jogcím törzs ([dd2a27b](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/dd2a27bad5ecdf1d36138ffad331abb566feff2a) by lordoftheflies). Related issues/PRs: #87
- Feat(masterdata): szociális hozzájárulás kedvezmény törzs ([8302048](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/8302048037abf7e4ace4c00feba0f6a4a1750245) by lordoftheflies). Related issues/PRs: #82
- Refactor: add common module ([094577f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/094577fa220cf8cf7b48afd4341f89641d510f71) by lordoftheflies).
- Feat(flatpage): statikus, de aminisztráció felől szerkeszhető. ([d5cf343](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d5cf34365961be54c297dd0ed12e7da0d1392d08) by lordoftheflies).
- Chore(perf): loggolás, rss, és alapoldalak. ([539f65f](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/539f65fbdcf290726cb55f0d66ef3a2c52e4133b) by lordoftheflies).
- Chore(integrity): nem null engedeélyezése. ([9fc1059](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/9fc105946c1d936c5d958abb2a864ef5fb5033d6) by lordoftheflies). Related issues/PRs: #28
- Chore(upload): fix file upload ([2e7d858](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/2e7d8580f825661b334807f0910cb0badbf331b8) by lordoftheflies).
- Chore(rest): add sitemap and rest. ([ea21078](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/ea210781e3c091b36aeb573be87d3e3300b9422f) by lordoftheflies).


## [v0.3.6](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v0.3.6) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v0.3.5...v0.3.6)) - 2020-05-29

### Fixed
- Fix issue template ([d75c728](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/d75c728b67b219f6fcae4ea94d77a08295776105) by László Hegedűs).

### Misc
- "[skip ci] bump version: 0.3.5 → 0.3.6" ([1edcc0c](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/1edcc0ceb607ff43505393056f66f0e56a14493f) by lordoftheflies).
- [skip ci] update changelog for \#572553384. ([b28bdc8](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/b28bdc8a2dc5b5ecd64e251202bb47404443ad26) by lordoftheflies).
- Chore(i18n): függ. tev nyilatkozat költség ([eb6da32](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/eb6da3258b05fc15f8d7fa0e25b299b18d3aa653) by László Hegedűs). Related issues/PRs: #59
- Feat(master-data): céggormák torzs ([abb5b30](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/abb5b309b14c3acfbe3d40a8f2e19212330aaeea) by László Hegedűs). Related issues/PRs: #52


## [v0.3.5](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v0.3.5) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/v0.3.4...v0.3.5)) - 2020-05-25

### Fixed
- Fix(dataloading): add fixture to manifest. ([aabe9b3](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/aabe9b35ef90ba41c0cbe9053cce362c44946088) by László Hegedűs).

### Misc
- "[skip ci] bump version: 0.3.4 → 0.3.5" ([e6a5940](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/e6a59406eae29d91ca5f3e22c574e2b0d2e57be6) by lordoftheflies).
- [skip ci] update changelog for \#566773257. ([6da71ae](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/6da71ae7ee7fd59e4979ffa4bbda9094cc7d5e81) by lordoftheflies).


## [v0.3.4](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/tags/v0.3.4) ([compare](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/compare/cc600dd347e2a257f095bc97984732e3878f99c0...v0.3.4)) - 2020-05-25

### Added
- Add release to sentry configuration ([68ceefe](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/68ceefe82efab6d71aa9574f2eeb503b87c55d2a) by László Hegedűs).

### Fixed
- Fix(layout): fix view layout ([26cbc2e](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/26cbc2ea4093064c154115f0d6de79b51ddd8e3d) by László Hegedűs).
- Fix(i18n): fix localization. ([4ae0163](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4ae016359b47d3cfd362fe3192af5e14898f1aa7) by László Hegedűs).
- Fix(perf): reinitialize data ([1564886](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/15648864e51d4c62d79b422362340ea784379da5) by László Hegedűs).

### Misc
- "[skip ci] bump version: 0.3.3 → 0.3.4" ([4e245ce](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4e245ce30e78c3229c639521181405bcf18965b3) by lordoftheflies).
- [skip ci] update changelog for \#566698847. ([4a10759](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/4a10759359ed6ff79ba3b8f33d08a181e7861bf7) by lordoftheflies).
- Chore(i18n): translations ([edabcd9](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/edabcd9a51ff8c0f5a1c09835ceb8be9ce55d323) by László Hegedűs).
- "[skip ci] bump version: 0.2.0 → 0.3.0" ([fdb66f7](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/fdb66f71b1bbbedd2d8e14f820aa608a02c9b00e) by lordoftheflies).
- "[skip ci] bump version: 0.1.11 → 0.2.0" ([edd862d](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/edd862dda864333cc041a065d625e13190a324d6) by lordoftheflies).
- [skip ci] update changelog for \#. ([6ea6231](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/6ea623131f000de1be85409cc5624273e771be98) by lordoftheflies).
- Extend logging. ([032c158](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/032c15811508c6f046f3de7764d4866841afd146) by László Hegedűs).
- Missing dependencies. ([96860c4](https://gitlab-ci-token:WaxN_vmfKr3oyWsK2yrv@gitlab.com/corgy/corgy-erp/commit/96860c4cb3c03278cc78ba826867a4ed309767f0) by László Hegedűs).


